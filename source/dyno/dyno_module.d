module dyno.dyno_module;

import std.string;

import dyno.injected;


class Module
{
    private Bean[string] services;
    private Controller[string] controllers;
    private Module[string] modules;

    protected void addService(T = Bean)(string name) {
        this.services[name] = new T();
    }

    protected void addController(T = Controller)(string name) {
        this.controllers[name] = new T();
    }

    protected void addModule(T = Module)(string name) {
        this.modules[name] = new T();
    }

    public void moduleLoadEnd() {
        foreach (Bean t; this.services) {
            t.loadEnd();
        }

        foreach (Controller t; this.controllers) {
            t.loadEnd();
        }

        foreach (Module t; this.modules) {
            t.moduleLoadEnd();
        }
    }

    
}