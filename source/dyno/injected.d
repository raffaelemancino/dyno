module dyno.injected;

class Injectable
{
    public void loadEnd() {}
}

class Bean: Injectable
{
    
}

class Component: Bean
{
    
}

class Service: Component
{
    
}

class Controller: Component
{
    
}