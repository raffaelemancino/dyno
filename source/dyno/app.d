module dyno.app;

import std.stdio;

import dyno.dyno_module;


class App
{
    private Module boot;

    this() {
        writeln("Creating Dyno App...");
    }

    public void bind() {

    }

    public void bootstrap(T = Module) () {
        this.boot = new T();
        this.boot.moduleLoadEnd();
    }
}